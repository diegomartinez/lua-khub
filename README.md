
# KHub

TODO: Summary.

## License

This library is Free Software released under a MIT-like license.
See @{LICENSE.md} for details.

## Requirements

* [Lua][lua] 5.1 or above.
* [luasocket][luasocket].

[lua]: http://lua.org
[luasocket]: https://github.com/diegonehab/luasocket
