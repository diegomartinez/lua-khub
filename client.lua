
---
-- Client implementation.
--
-- **Extends:** `klass`
--
-- @classmod khub.client

local socket = require "socket"
local common = require "khub.common"

local klass = require "klass"
local client = klass:extend("khub.client")

---
-- Application identifier.
--
-- @tfield string appid Default is nil.
client.appid = nil

---
-- Human-readable name.
--
-- @tfield string name Default is the empty string.
client.name = ""

---
-- Remote host.
--
-- @tfield string host Default is nil.
client.host = ""

---
-- Remote port.
--
-- @tfield number port Default is nil.
client.port = nil

---
-- Socket used for communication.
--
-- @tfield socket socket Created by the `listen` method.
client.socket = nil

---
-- List of users.
--
-- Values are `khub.user` tables. This table may be indexed by user ID
-- or by the user table itself.
--
-- @tfield table users Initialized by constructor to an empty table.
client.users = nil

---
-- Send buffer.
--
-- @tfield string buffer Default is the empty string.
client.buffer = ""

---
-- Send buffer pos.
--
-- @tfield number bufferpos
client.bufferpos = 1

---
-- Registered commands.
--
-- Mapping of `string` to `function(client, ...)`.
--
-- @tfield table commands Default is an empty table.
client.commands = ({ })

---
-- Step interval in seconds.
--
-- The amount specified here is passed to `socket.sleep` each step
-- by the `mainloop` method.
--
-- @tfield number stepinterval Default is 0.
client.stepinterval = 0

---
-- Timeout in seconds.
--
-- TODO: Expand doc.
--
-- @tfield number timeout Default is 5.
client.timeout = 5

---
-- Ping sequence.
--
-- TODO: Expand doc.
--
-- @tfield number pingseq Default is 0.
client.pingseq = 0

---
-- Constructor.
--
-- TODO: Expand doc.
--
-- @tparam ?string host Address. If not nil, `host` is set to that value.
-- @tparam ?number port Port. If not nil, `port` is set to that value.
function client:init(host, port)
	if host ~= nil then
		self.host = host
	end
	if port ~= nil then
		self.port = port
	end
	self.users = { }
	self.commands = setmetatable({ }, { __index=self.commands })
end

---
-- TODO: Doc.
--
-- @tparam string name
-- @tparam function func `function(client, ...) --> ok, errmsg`
function client:registercommand(name, func)
	self.commands[name] = func
end

local function hcbail(ok, ...)
	if ok == nil then
		ok = true
	end
	return ok, ...
end

---
-- TODO: Doc.
--
-- @tparam string cmd
-- @tparam string ...
-- @treturn boolean
function client:handlecommand(cmd, ...)
	if type(cmd) ~= "string" then
		return self:onjunk(cmd, ...)
	end
	local func = self.commands[cmd]
	if func then
		return hcbail(func(self, ...))
	end
	return hcbail(self:onunknowncommand(cmd, ...))
end

---
-- TODO: Doc.
--
-- @tparam string cmd
-- @tparam any ...
-- @treturn boolean
function client:onunknowncommand(cmd, ...) -- luacheck: ignore
end

---
-- TODO: Doc.
--
-- @tparam ?string host
-- @tparam ?number port
function client:connect(host, port)
	assert(self.appid, "appid must be set before connection")
	assert(not self.socket, "already connected")
	host = host or self.host
	port = port or self.port
	local sock, ok, err
	sock, err = socket.tcp()
	if not sock then
		return nil, err
	end
	ok, err = sock:connect(host, port)
	if not ok then
		return nil, err
	end
	self.socket = sock
	self.host, self.port = host, port
	self:send(":hello", self.appid, self.name)
	local start = socket.gettime()
	while not self.id do
		if socket.gettime()-start > self.timeout then
			self.socket:close()
			self.socket = nil
			return nil, "init timeout"
		end
		ok, err = self:step()
		if not ok then
			return nil, err or "closed"
		end
		socket.sleep(self.stepinterval)
	end
	return true
end

---
-- Send raw data through the socket.
--
-- @tparam string data Data to send.
-- @treturn number Number of sent bytes on success, nil on error.
-- @treturn ?string Error message.
-- @treturn ?number Index of last sent byte if applicable.
function client:sendraw(data)
	return self.socket:send(data)
end

---
-- Send data through the socket.
--
-- The data is serialized before sending.
--
-- @tparam nil|number|string|boolean ... Values to send.
-- @treturn boolean True on success, nil on error.
-- @treturn ?string Error message.
-- @treturn ?string Rest of buffer on timeout.
-- @see sendraw
-- @see khub.serial
function client:send(...)
	if not self.socket then
		return nil, "not connected"
	end
	return common.sendto(self.socket, ...)
end

---
-- TODO: Doc.
--
-- @tparam ?number target
-- @tparam string message
-- @treturn boolean True on success, nil on error.
-- @treturn ?string Error message.
-- @treturn ?string Rest of buffer on timeout.
function client:sendmessage(target, message)
	if message == nil then
		target, message = nil, target
	end
	return self:send(":msg", target, message)
end

---
-- TODO: Doc.
--
-- @treturn boolean ok
-- @treturn ?string error
function client:receive()
	if not self.socket then
		return nil, "not connected"
	end
	return common.receivefrom(self.socket)
end

---
-- TODO: Doc.
--
-- @treturn boolean cont
function client:step()
	if not self.socket then
		return nil, "not connected"
	end
	repeat
		local rl = socket.select({ self.socket }, nil, 0)
		if rl[self.socket] then
			local ok, data, err
			data, err = self:receive()
			if data then
				ok, err = self:handlecommand(unpack(data, 1, data.n))
				if ok == false
						and self:onerror(err, "handlecommand") == false then
					return nil, err
				end
			elseif err == "closed" then
				self:shutdown()
			elseif self:onerror(err, "receive") == false then
				return nil, err
			end
		end
	until not (self.socket and rl[self.socket])
	return self.socket and true or false
end

---
-- TODO: Doc.
--
-- @tparam function stepfunc `function(self) --> cont=true`
-- @treturn boolean|nil
--  True on success, nil on error.
-- @treturn string
--  Error message.
function client:mainloop(stepfunc)
	if not self.socket then
		local ok, err = self:connect()
		if not ok then
			return nil, err
		end
	end
	while self:step() do
		if stepfunc and stepfunc(self) == false then
			break
		end
		socket.sleep(self.stepinterval)
	end
	self:shutdown()
	return true
end

---
-- TODO: Doc.
--
-- @tparam ?string reason
function client:shutdown(reason)
	if not self.socket then
		return
	end
	local sock = self.socket
	self:send(":quit", reason)
	self.socket = nil
	self.id = nil
	sock:close()
end

---
-- TODO: Doc.
--
-- @tparam number id
-- @tparam string name
-- @tparam string desc
function client:onwelcome(id, name, desc) -- luacheck: ignore
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
function client:onjoin(user) -- luacheck: ignore
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam string reason
function client:onquit(user, reason) -- luacheck: ignore
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam boolean ispriv
-- @tparam string message
function client:onmessage(user, ispriv, message) -- luacheck: ignore
end

---
-- TODO: Doc.
--
function client:onping()
end

---
-- TODO: Doc.
--
-- @tparam number pingtime
function client:onpong(pingtime) -- luacheck: ignore
end

---
-- Called when an error occurs.
--
-- @tparam string err
-- @tparam string context The context of the error. This can be `"send"` for
--  errors that occur while sending
-- @treturn nil|boolean
function client:onerror(err, context) -- luacheck: ignore
end

---
-- Called when junk data is received.
--
-- @tparam any ... Junk data.
function client:onjunk(...) -- luacheck: ignore
end

client:registercommand(":welcome", function(self, id, name, desc)
	if type(id) ~= "number" then
		self:shutdown("invalid ID on welcome")
		return
	end
	self.id = id
	name = name ~= nil and tostring(name) or nil
	desc = desc ~= nil and tostring(desc) or nil
	self:onwelcome(name, desc)
	self:send(":join", self.name)
end)

client:registercommand(":join", function(self, id, name)
	if type(id) ~= "number"
			or (name ~= nil and type(name) ~= "string") then
		return self:onjunk(":join", id, name)
	end
	local user = self.users[id]
	if not user then
		user = { }
		self.users[id] = user
	end
	user.id = id
	user.name = name
	return self:onjoin(user)
end)

client:registercommand(":quit", function(self, id, reason)
	if (id ~= nil and type(id) ~= "number")
			or (reason ~= nil and type(reason) ~= "string") then
		return self:onjunk(":quit", id, reason)
	elseif id == nil or id == self.id then
		self:shutdown(reason)
		return
	end
	local user = self.users[id]
	if not user then
		return
	end
	self.users[id] = nil
	return self:onquit(user, reason)
end)

client:registercommand(":msg", function(self, source, ispriv, message)
	if type(source) ~= "number"
			or type(ispriv) ~= "boolean"
			or type(message) ~= "string" then
		return self:onjunk(":msg", source, ispriv, message)
	end
	local user = self.users[source]
	if not user then
		return
	end
	self:onmessage(user, ispriv, message)
end)

client:registercommand(":ping", function(self, seq)
	if type(seq) ~= "number" then
		return self:onjunk(":ping", seq)
	end
	self:onping()
	self:send(":pong", seq)
end)

client:registercommand(":pong", function(self, seq)
	if type(seq) ~= "number" then
		return self:onjunk(":pong", seq)
	end
	if seq == self.pingseq then
		self:onpong(socket.gettime() - self.pingtime)
	end
end)

function client:__tostring()
	return (("<%s%s%s>"):format(
			self.__name,
			self.id and " id="..self.id or "",
			self.name and self.name~=""
				and (" name=%q"):format(self.name)
				or ""))
end

return client
