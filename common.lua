
---
-- Common functions for both client and server.
--
-- @module khub.common

local minser = require "minser"

local common = { }

---
-- Send data through the socket.
--
-- The data is serialized before sending.
--
-- @tparam socket sock Socket to send through.
-- @tparam nil|number|string|boolean ... Values to send.
-- @treturn boolean True on success, nil on error.
-- @treturn ?string Error message.
-- @treturn ?string Rest of buffer on timeout.
function common.sendto(sock, ...)
	local data = assert(minser.dump(...)).."\n"
	local ok, err, sent = sock:send(data)
	if ok then
		return ok
	elseif err == "timeout" then
		return nil, err, data:sub(sent+1)
	else
		return nil, err
	end
end

local function bail(ok, ...)
	if not ok then
		return nil, ...
	end
	local data = { n=select("#", ...), ... }
	return data
end

---
-- Receive data from the socket.
--
-- @tparam socket sock Socket to receive from.
-- @treturn boolean True on success, nil on error.
-- @treturn ?any|string error First value (if any) on success,
--  error message on error.
-- @treturn ?any Extra values are returned as extra results.
function common.receivefrom(sock)
	local data, err = sock:receive("*l")
	if not data then
		return nil, err
	end
	return bail(minser.load(data))
end

return common
