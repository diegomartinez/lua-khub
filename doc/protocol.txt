
The protocol uses values (de)serialized with `minser`.
The basic block is a line terminated with "\n".
Each line should be `command, val1, val2, ... valN`

legend:
--> Client-server comm.
<-- Server-client comm.
>>> (target) Server-(target) comm.
*** Broadcast (server to all except source).
+++ Other actions.

connect:
--> "hello", appid, name
+++ If appid does not match, drops connection.
<-- "welcome", id, servername, serverdesc
*** "join", id, name

disconnect:
--> "quit", reason
<-- "quit", reason
*** "quit", reason
+++ Drops connection.

global message:
--> "msg", nil, text
*** "msg", source, false, text

private message:
--> "msg", target, text
>>> (target) "msg", source, true, text

ping:
<-- "ping"
--> "pong"
