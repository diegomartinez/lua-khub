#! /usr/bin/env lua

local BASE = debug.getinfo(1).source:gsub("^@", ""):match("^(.*)[/\\]") or "."
package.path = BASE.."/?.lua;"..BASE.."/?/init.lua;"..package.path

local khub = require "khub"
local excommon = require "excommon"

local client = khub.client()

function client:onwelcome()
	excommon.printf("*** Connected. Your peer ID is #%d.", self.id)
	excommon.printf("*** %s", self.servername
			and self.servername:gsub("\n", "\n*** ")
			or "No name.")
	excommon.printf("*** %s", self.serverdescription
			and self.serverdesc:gsub("\n", "\n*** ")
			or "No description.")
end

function client:onuserjoin(user)
	excommon.printf("* %s joined.", excommon.userident(user))
end

function client:onuserquit(user)
	excommon.printf("* %s quit.", excommon.peerident(user))
end

function client:onmessage(user, ispriv, message)
	if ispriv then
		excommon.printf(">%s< %s", excommon.userident(user), message)
	else
		excommon.printf("<%s> %s", excommon.userident(user), message)
	end
end

local host = "localhost"

for port = 1234, 1238 do
	local ok, err = client:connect(host, port)
	if ok then
		excommon.printf("*** Connected to %s:%d.", host, port)
		ok, err = client:mainloop(function()
			local line = excommon.prompt()
			if (not line) or line == "/q" or line == "/quit" then
				client:shutdown()
				return false
			elseif line == "/p" or line == "/ping" then
				client:send("ping")
			else
				client:send("msg", 0, line)
			end
		end)
		if ok then
			excommon.printf("*** Done.")
			break
		end
	end
	if err then
		excommon.printf("*** Error on %s:%d: %s", host, port, err)
	end
end
