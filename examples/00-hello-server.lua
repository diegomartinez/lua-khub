#! /usr/bin/env lua

local BASE = debug.getinfo(1).source:gsub("^@", ""):match("^(.*)[/\\]") or "."
package.path = BASE.."/?.lua;"..BASE.."/?/init.lua;"..package.path

local khub = require "khub"
local excommon = require "excommon"

local server = khub.server()

function server:onaccept(client)
	excommon.printf("* %s:%d accepted.", client.host, client.port)
end

function server:onremove(client)
	excommon.printf("* %s:%d removed.", client.host, client.port)
end

function server:onerror(err)
	excommon.printf("*** ERROR: %s.", err)
	excommon.printf("*** "..debug.traceback():gsub("\n", "\n*** "))
end

function server:onmessage(client, target, message)
	if target then
		excommon.printf("<%s|%s> %s",
				excommon.clientident(client),
				excommon.clientident(target),
				message)
	else
		excommon.printf("<%s> %s",
				excommon.clientident(client),
				message)
	end
end

local host = "0.0.0.0"

for port = 1234, 1238 do
	local ok, err = server:listen(host, port)
	if ok then
		excommon.printf("*** Listening on %s:%d.", host, port)
		server:mainloop()
		excommon.printf("*** Done.")
		break
	else
		excommon.printf("*** Error on %s:%d: %s.", host, port, err)
	end
end
