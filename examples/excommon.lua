
local excommon = { }

excommon.ps1 = "> "

function excommon.printf(fmt, ...)
	return io.write(fmt:format(...), "\n")
end

function excommon.prompt()
	io.write(excommon.ps1)
	io.flush()
	return io.read()
end

return excommon
