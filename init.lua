
---
-- Main K-Hub module.
--
-- Submodules are exported as fields of this module.
--
-- @module khub
-- @see khub.common
-- @see khub.client
-- @see khub.server

local khub = { }

khub.common = require "khub.common"
khub.client = require "khub.client"
khub.server = require "khub.server"

---
-- TODO: Doc.
--
-- @table user
-- @tfield number id
-- @tfield string name
-- @tfield string host
--  **Not available client-side.**
-- @tfield number port
--  **Not available client-side.**
-- @tfield socket socket
--  **Not available client-side.**

return khub
