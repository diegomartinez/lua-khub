
---
-- Server implementation.
--
-- Note: Calling the module has the same effect as calling the `new` method.
--
-- @classmod khub.server

local socket = require "socket"
local common = require "khub.common"

local klass = require "klass"
local server = klass:extend("khub.server")

---
-- Application identifier.
--
-- @tfield string appid Default is nil.
server.appid = nil

---
-- Human-readable server name.
--
-- @tfield string name Default is the empty string.
server.name = ""

---
-- Human-readable description.
--
-- @tfield string description Default is the empty string.
server.description = ""

---
-- Local address to bind to.
--
-- @tfield ?string host Default is nil.
server.host = nil

---
-- Local port to bind to.
--
-- @tfield ?number port Default is nil.
server.port = nil

---
-- Array of connected users.
--
-- Values are `khub.user` tables. This table may be indexed by user ID,
-- by socket, or by the user table itself.
--
-- @tfield table users Initialized by constructor to an empty table.
server.users = nil

---
-- Registered commands.
--
-- @tfield table commands Mapping of `string` to
--  `function(server, user, ...)`.
-- @see khub.servercommands
server.commands = ({ })

---
-- Socket for listening.
--
-- @tfield socket socket Default is nil.
server.socket = nil

---
-- TODO: Doc.
--
-- @tfield number stepinterval Default is 0.
server.stepinterval = 0

---
-- TODO: Doc.
--
-- @tfield number timeout Default is 60.
server.timeout = 60

---
-- TODO: Doc.
--
-- @tfield number pinginterval Default is 15.
server.pinginterval = 15

---
-- Constructor.
--
-- @tparam ?string host Address. If not nil, `host` is set to that value.
-- @tparam ?number port Port. If not nil, `port` is set to that value.
function server:init(host, port)
	if host ~= nil then
		self.host = host
	end
	if port ~= nil then
		self.port = port
	end
	self.users = { }
	self.commands = setmetatable({ }, { __index=self.commands })
end

---
-- Iterate over connected users.
--
-- @treturn function
--  An iterator function yielding `khub.user`.
function server:iterusers()
	local i = 1
	return function()
		while i <= table.maxn(self.users) do
			local user = self.users[i]
			i = i + 1
			if user then
				return user
			end
		end
	end
end

---
-- @tparam string name
-- @tparam function func `function(server, user, ...) --> ok, errmsg`
function server:registercommand(name, func)
	self.commands[name] = func
end

local function hcbail(ok, ...)
	if ok == nil then
		ok = true
	end
	return ok, ...
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam string cmd
-- @tparam string ...
-- @treturn boolean
function server:handlecommand(user, cmd, ...)
	cmd = tostring(cmd)
	local func = self.commands[cmd]
	if func then
		return hcbail(func(self, user, ...))
	end
	return hcbail(self:onunknowncommand(user, cmd, ...))
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam string cmd
-- @tparam any ...
-- @treturn boolean
function server:onunknowncommand(user, cmd, ...) -- luacheck: ignore
end

---
-- Send data to an user.
--
-- @tparam khub.user|number|socket user User to send to.
-- @tparam nil|number|string|table|boolean ... Data to send.
function server:sendto(user, ...)
	user = self.users[user]
	if not user then
		return nil, "no such user"
	end
	return common.sendto(user.socket, ...)
end

---
-- Send a message to an user.
--
-- @tparam khub.user|number|socket user User to send to.
-- @tparam string message Message body.
function server:sendmessageto(user, message)
	return self:sendto(user, ":msg", message)
end

---
-- TODO: Doc.
--
-- @tparam khub.user|number|socket skip
-- @tparam nil|number|string|table|boolean ... Data to send.
function server:broadcast(skip, ...)
	for user in self:iterusers() do
		if ((skip == nil) or user ~= self.users[skip])
				and user.socket then
			common.sendto(user.socket, ...)
		end
	end
end

---
-- Start listening for connections.
--
-- This method creates a socket, binds it to the specified address and port,
-- and marks it as ready to accept connections. It is called automatically by
-- `mainloop` if necessary, but you should call it yourself if you're calling
-- `step` directly in your own loop.
--
-- @tparam ?string host Address. If nil, defaults to `host`.
-- @tparam ?number port Port. If nil, defaults to `port`.
-- @tparam ?number backlog Number of user connections that can be queued
--  waiting for service. If nil, defaults to `backlog`.
-- @treturn boolean
--  True on success, nil on error.
-- @treturn ?string
--  Error message.
function server:listen(host, port, backlog)
	assert(not self.socket, "already bound")
	host = host or self.host
	port = port or self.port
	local sock, ok, err
	sock, err = socket.tcp()
	if not sock then
		return nil, err
	end
	ok, err = sock:bind(host, port)
	if not ok then
		return nil, err
	end
	ok, err = sock:listen(backlog)
	if not ok then
		return nil, err
	end
	sock:settimeout(0)
	self.socket = sock
	return true
end

local function accept(self, user)
	for id = 1, table.maxn(self.users)+1 do
		if not self.users[id] then
			user.id = id
			self.users[user.id] = user
			self.users[user.socket] = user
			self.users[user] = user
			break
		end
	end
end

---
-- Perform a server step.
--
-- This function handles new connections, and updates existing connections.
--
-- @treturn boolean
--  True if the server is still running, false if it was shut down, nil on
--  unhandled errors.
-- @treturn string
--  Error message.
function server:step()
	if not self.socket then
		return nil, "not bound"
	end
	while true do
		local sock, err = self.socket:accept()
		if not sock then
			if err == "timeout" then
				break
			elseif self:onerror(err, "accept") == false then
				self:shutdown()
				return nil, err
			end
		end
		local host, port = sock:getpeername()
		local user = { }
		user.host = host
		user.port = port
		user.socket = sock
		user.lastrecv = socket.gettime()
		local ok, e = self:onaccept(user)
		if ok ~= false then
			accept(self, user)
		else
			self:onerror(e)
		end
	end
	local trl = { }
	for user in self:iterusers() do
		if user.socket then
			trl[#trl+1] = user.socket
			local dt = socket.gettime()-user.lastrecv
			if dt > self.timeout then
				self:remove(user, "ping timeout")
			elseif dt > self.pinginterval then
				user.pingseq = (user.pingseq or 0) + 1
				user.pingtime = socket.gettime()
				self:sendto(user, ":ping", user.pingseq)
			end
		end
	end
	local rl, _, err = socket.select(trl, nil, 0)
	if rl then
		for _, sock in ipairs(rl) do
			local user = self.users[sock]
			if user then
				local data
				data, err = common.receivefrom(user.socket)
				if data then
					user.lastrecv = socket.gettime()
					local ok
					ok, err = self:handlecommand(
							user, unpack(data, 1, data.n))
					if ok == false and self:onerror(
							err, "handlecommand") == false then
						self:shutdown()
						return nil, err
					end
				elseif err == "closed" then
					self:remove(self, user, err)
				elseif self:onerror(err, "receive") == false then
					self:shutdown()
					return nil, err
				end
			end
		end
	elseif err ~= "timeout"
			and self:onerror(err, "select") == false then
		return nil, err
	end
	return self.socket ~= nil
end

---
-- TODO: Doc.
--
-- @tparam function stepfunc `function(self) --> cont=true`
-- @treturn boolean|nil
--  True on success, nil on error.
-- @treturn string
--  Error message.
function server:mainloop(stepfunc)
	if not self.socket then
		local ok, err = self:listen()
		if not ok then
			return nil, err
		end
	end
	while self:step() do
		if stepfunc then
			local ok, err = stepfunc(self)
			if ok == false then
				return ok, err
			end
		end
		socket.sleep(self.stepinterval)
	end
	self:shutdown()
	return true
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam string reason
function server:remove(user, reason)
	user = self.users[user]
	if not user then
		return
	end
	if user.joined then
		self:broadcast(user, ":quit", user.id, reason)
	end
	local sock = user.socket
	self.users[user.id] = nil
	self.users[sock] = nil
	self.users[user] = nil
	common.sendto(sock, ":quit", user.id, reason)
	sock:close()
	self:onquit(user, reason)
end

---
-- Shut down the server.
--
-- Removes all users, then shuts down the socket.
--
-- @tparam ?string reason Reason for shutting down.
--  Defaults to the empty string.
-- @see remove
function server:shutdown(reason)
	if not self.socket then
		return
	end
	for user in self:iterusers() do
		self:remove(user, reason)
	end
	self.socket:close()
	self.socket = nil
end

---
-- Called when an user connects.
--
-- The default implementation does nothing.
--
-- @tparam ?khub.user user The user.
--  Note that the `id` field is not valid yet.
-- @treturn boolean
--  If an explicit false is returned, the user is disallowed to join.
-- @treturn ?string
--  Error message.
function server:onaccept(user) -- luacheck: ignore
end

---
-- Called when an user joins the server.
--
-- The default implementation does nothing.
--
-- @tparam khub.user user The user.
function server:onjoin(user) -- luacheck: ignore
end

---
-- TODO: Doc.
--
-- @tparam ?khub.user user The user.
--  Note that the `id` field is not valid yet.
-- @treturn boolean
--  If an explicit false is returned, the user is disallowed to join.
-- @treturn ?string
--  Error message.
function server:onquit(user) -- luacheck: ignore
end

---
-- Called when an user quits.
--
-- The default implementation does nothing.
--
-- @tparam khub.user user The user.
-- @tparam ?string reason Reason for quitting.
function server:onquit(user, reason) -- luacheck: ignore
end

---
-- Called when an user sends a message.
--
-- The default implementation does nothing.
--
-- @tparam khub.user source The source user.
-- @tparam khub.user|nil target The target user.
--  It is nil if the message is sent to all users.
-- @tparam string message The message.
function server:onmessage(source, target, message) -- luacheck: ignore
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
function server:onping(user) -- luacheck: ignore
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
function server:onpong(user) -- luacheck: ignore
end

---
-- Called when an error occurs.
--
-- The default implementation does nothing.
--
-- @tparam string err Error message.
-- @tparam string context The context of the error.
function server:onerror(err, context) -- luacheck: ignore
end

server:registercommand(":hello", function(self, user, appid, name)
	if appid ~= self.appid then
		self:remove(user, "wrong appid on hello")
	end
	user.joined = true
	common.sendto(user.socket, ":welcome",
			user.id, self.name, self.description)
	user.name = name ~= nil and tostring(name) or "User"..user.id
	for u in self:iterusers() do
		if u ~= user then
			common.sendto(u.socket, ":join", user.id, user.name)
			common.sendto(user.socket, ":join", u.id, u.name)
		end
	end
	self:onjoin(user)
end)

server:registercommand(":quit", function(self, user, reason)
	reason = reason ~= nil and tostring(reason) or nil
	self:remove(user, reason)
end)

server:registercommand(":msg", function(self, user, target, message)
	if (target ~= nil and type(target) ~= "number")
			or type(message) ~= "string" then
		return self:onjunk(user, ":msg", target, message)
	elseif target == nil or target == 0 then
		local ok = self:onmessage(user, nil, message)
		if ok == false then
			return
		end
		self:broadcast(user, ":msg", user.id, false, message)
	else
		target = self.users[target]
		if target then
			local ok = self:onmessage(user, target, message)
			if ok == false then
				return
			end
			self:sendto(target, ":msg", user.id, true, message)
		end
	end
end)

server:registercommand(":ping", function(self, user, seq)
	if type(seq) ~= "number" then
		return self:onjunk(user, ":ping", seq)
	end
	self:onping(user)
	self:sendto(user, ":pong", seq)
end)

server:registercommand(":pong", function(self, user, seq)
	if type(seq) ~= "number" then
		return self:onjunk(user, ":pong", seq)
	end
	if seq == user.pingseq then
		self:onpong(user, socket.gettime() - user.pingtime)
	end
end)

function server:__tostring()
	return "<khub.server>"
end

return server
